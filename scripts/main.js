class BaseActorSheet extends ActorSheet {
/**
   * Récupère les options par defaut et les ecrases.
   */
  constructor(actor, options) {
    super(actor, options);
  }

  static get defaultOptions () {
    const options = super.defaultOptions;
    options.classes = options.classes.concat(["ares"]);
    options.template = 'public/systems/ares/templates/actor-sheet.html';
    options.width = 600; // This configures the default starting width
    options.height = 720; // Starting height
    options.submitOnUnfocus = true;  // Should the form be saved when an input field is unfocused?
    options.submitOnClose = true;  // Should the form be saved when the sheet is closed?
    options.closeOnSubmit = false;  // Should the sheet be closed when it is submitted?
    options.resizable = true;  // Should the sheet be resizable?
    return options;
  }
  

  getData() {
    const data = super.getData();
    data.actor = data.entity;
    data.data = data.entity.data;
    data.actorColor = game.user.color;
    return data;
  }

  activateListeners(html) {
    super.activateListeners(html);
  }

}
function ColorAlert(){
  alert (data.actorColor);
}

//Desactive la sheet par defaut et enregistre une nouvelle fiche
Actors.unregisterSheet("core", ActorSheet);
Actors.registerSheet("core", BaseActorSheet, {
  types: [],
  makeDefault: true
});